from django.db import models

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=200)


class Hats(models.Model):
    """
    this is my model for hats
    """
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    picture = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="shoes",
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.style_name
