from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import LocationVO, Hats


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "closet_name",
        "section_number",
        "shelf_number",
    ]


class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "picture",
        "id",
        # "location"
    ]
    # encoders = {
    #     "location": LocationVOEncoder(),
    # }

    def get_extra_data(self, o):
        return {
            "closet_name": o.location.closet_name,
            }

    # def get_extra_data(self, o):
    #     return {"section_number": o.location.section_number}

    # def get_extra_data(self, o):
    #     return {"shelf_number": o.location.shelf_number}


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "picture",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}


@require_http_methods(["GET", "POST"])
def list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
        )
    else:
        content = json.loads(request.body)
        location_href = content["location"]
        location = LocationVO.objects.get(import_href=location_href)
        content["location"] = location

        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatsListEncoder,
            safe=False
        )


def hat_detail(request, pk):
    if request.method == "GET":
        hat = Hats.objects.get(id=pk)
        return JsonResponse(
            {"hat": hat},
            encoder=HatDetailEncoder,
            safe=False
        )
    else:
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
