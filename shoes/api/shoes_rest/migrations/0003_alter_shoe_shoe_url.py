# Generated by Django 4.0.3 on 2023-07-19 21:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_alter_shoe_bin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoe',
            name='shoe_url',
            field=models.CharField(max_length=200, null=True),
        ),
    ]
