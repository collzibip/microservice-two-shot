from django.urls import path

from .views import api_list_shoes, api_shoe_detail

urlpatterns = [
    path("shoes_rest/", api_list_shoes, name="api_create_shoes"),
    path(
        "bins/<int:biv_vo_id>/shoes/",
        api_list_shoes,
        name="api_list_shoes",
    ),
    path("shoes_rest/<int:id>", api_shoe_detail, name="api_shoe_detail")
]
