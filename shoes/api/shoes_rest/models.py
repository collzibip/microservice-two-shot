from django.db import models
from django.urls import reverse

class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    bin_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return str(self.bin_number)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    shoe_url = models.CharField(max_length=200, null=True)
    model = models.CharField(max_length=200, null=True, blank=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.manufacturer} {self.model}"

    def get_api_url(self):
        return reverse("api_show_shoe", kwargs={"id": self.id})
