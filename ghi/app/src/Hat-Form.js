import React, {useEffect, useState } from 'react';
function HatForm(){
    const [fabric, setFabric] = useState([])
    const [style, setStyle] = useState([])
    const [picture, setPicture] = useState([])
    const [location, setLocation] = useState('')
    const [locations, setLocations] = useState([])

    const fetchData = async () => {
        const response = await fetch('http://localhost:8100/api/locations/')

        if (response.ok){
            const data = await response.json()
            console.log(data.locations)
            setLocations(data.locations)

        }
    }

    const handleStyleChange = (event) =>{
        const value = event.target.value
        setStyle(value)
    }
    const handleFabricChange = (event) =>{
        const value = event.target.value
        setFabric(value)
    }
    const handlePictureChange = (event) =>{
        const value = event.target.value
        setPicture(value)
    }
    const handleLocationChange = (event) =>{
        const value = event.target.value
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.fabric = fabric
        data.style_name= style
        data.picture = picture
        data.location = location

        const hatUrl = 'http://localhost:8090/hats/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(hatUrl, fetchConfig)
        if (response.ok){
            const newHat = await response.json()
            setFabric('')
            setLocation('')
            setPicture('')
            setStyle('')
        }
    }

    useEffect(() =>{
        fetchData();
    },[])

    return(
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Create a New Hat</h1>
                <form onSubmit={handleSubmit}  id="create-hat-form">
                  <div className="form-floating mb-3">
                    <input value = {style} onChange={handleStyleChange} placeholder="Style" required type="text" name = "style" id="style" className="form-control"/>
                    <label htmlFor="style">Style</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value ={fabric} onChange={handleFabricChange} placeholder="fabric" required type="text" name = "fabric" id="fabric" className="form-control"/>
                    <label htmlFor="fabric">Fabric</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input value ={picture} onChange={handlePictureChange} placeholder="picture url" required type="text" name = "picture" id="picture" className="form-control"/>
                    <label htmlFor="picture">picture url</label>
                  </div>
                  <div className="mb-3">
                    <select  value={location}  onChange={handleLocationChange} required id="location" name = "location" className="form-select">
                      <option value="">Choose a location</option>
                      {locations.map(location =>{
                        return(
                            <option key ={location.href} value = {location.href}>
                                {location.closet_name}
                            </option>
                        )
                      })}
                    </select>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
    )
}
export default HatForm
