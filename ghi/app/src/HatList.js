import React, {useEffect, useState} from 'react';
function HatList(){
    const[hats, setHats] = useState([])

    async function fetchHats(){
        const response = await fetch('http://localhost:8090/hats/')

        if (response.ok){
            const data = await response.json()
            console.log(data.hats)
            setHats(data.hats)
        }
    }

    const handleDelete = async (id) => {
      // Send a DELETE request to the API endpoint to delete the shoe
      const response = await fetch(`http://localhost:8090/hats/${id}/`, {
        method: 'delete',
      });
      if (response.ok) {
        // If the deletion was successful, update the list of shoes
        // by removing the deleted shoe from the state
        setHats(prevHats => prevHats.filter(hat => hat.id !== id));
      } else {
        // Handle the case where deletion failed (e.g., display an error message)
      }
    }
useEffect(() =>{
    fetchHats()
    }, [])

    return(
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Material</th>
            <th>Style</th>
            <th>Picture</th>
            <th>Closet Name</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td><img src={hat.picture} width="100px" /></td>
                <td>{ hat.closet_name}</td>
                <td>
                <button onClick={() => handleDelete(hat.id)} className="btn btn-primary">Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    )
}
export default HatList
