import React, {useEffect, useState} from 'react';
function ShoeList(){
    const[shoes, setShoes] = useState([])

    async function fetchShoes(){
        const response = await fetch('http://localhost:8080/shoes/')

        if (response.ok){
            const data = await response.json()
            console.log(data)
            setShoes(data.shoes)
        }
    }
useEffect(() =>{
    fetchShoes()
    }, [])

    return(
        <p>is this thing on?</p>
    )
}
export default ShoeList
