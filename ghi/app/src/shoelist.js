import React, { useEffect, useState } from 'react';

function ShoeList() {
  const [shoes, setShoes] = useState([]);

  async function fetchShoes() {
    const response = await fetch('http://localhost:8080/api/shoes_rest/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
    }
  }
  const handleDelete = async (id) => {
    // Send a DELETE request to the API endpoint to delete the shoe
    const response = await fetch(`http://localhost:8080/api/shoes_rest/${id}`, {
      method: 'DELETE',
    });

    if (response.ok) {
      // If the deletion was successful, update the list of shoes
      // by removing the deleted shoe from the state
      setShoes(prevShoes => prevShoes.filter(shoe => shoe.id !== id));
    } else {
      // Handle the case where deletion failed (e.g., display an error message)
    }
  };
  useEffect(() => {
    fetchShoes();
  }, []);

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Model</th>
          <th>Manufacturer</th>
          <th>Color</th>
          <th>Shoe Pic</th>
          <th>Bin</th>
          <th>Bin URL</th>
        </tr>
      </thead>
      <tbody>
        {shoes.map(shoe => (
          <tr key={shoe.id}>
            <td>{shoe.model}</td>
            <td>{shoe.manufacturer}</td>
            <td>{shoe.color}</td>
            <td><img src={shoe.shoe_url} width="100px" /></td>
            <td>{shoe.bin.bin_number}</td>
            <td>{shoe.bin.import_href}</td>
            <td>
              {/* Add a delete button with an onClick event handler */}
              <button onClick={() => handleDelete(shoe.id)}>Delete</button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default ShoeList;
