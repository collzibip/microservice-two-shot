import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './shoelist';
import HatList from './HatList';
import HatForm from './Hat-Form';

import ShoeForm from './shoeform';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList />} />
          <Route path="shoes/new" element={<ShoeForm/>}/>
          <Route path="hats/">
            <Route index element={<HatList/>}/>
            <Route path="new" element={<HatForm/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
