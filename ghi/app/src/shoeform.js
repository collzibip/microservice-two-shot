import React, { useState, useEffect } from 'react';

function ShoeForm() {


  const [bins, setBins] = useState([]);

  const[model, setModel] = useState([])
  const handleModelChange=(event)=>{
    const value = event.target.value
    setModel(value)
  }
  const[color, setColor] = useState([])
  const handleColorChange=(event)=>{
    const value = event.target.value
    setColor(value)
  }
  const[manufacturer, setManufacturer] = useState([])
  const handleManufacturerChange=(event)=>{
    const value = event.target.value
    setManufacturer(value)
  }
  const[bin, setBin] = useState('')
  const handleBinChange=(event)=>{
    const value = event.target.value
    setBin(value)
  }
  const[shoe_url, setUrl] = useState([])
  const handleShoeURlChange = (event) =>{
    const value = event.target.value
    setUrl(value)
}


  const fetchData = async () => {
    const url = 'http://localhost:8100/api/bins/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    }
  }



  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};

    data.bin = bin
    data.model = model;
    data.color = color;
    data.manufacturer = manufacturer;
    data.shoe_url = shoe_url;



    const shoesUrl = 'http://localhost:8080/api/shoes_rest/';

    const fetchConfig = {
      method: "post",
      //Because we are using one formData state object,
      //we can now pass it directly into our request!
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesUrl, fetchConfig)
    if (response.ok){
        const newShoe = await response.json()
        setManufacturer("")
        setColor("")
        setModel("")
        setUrl("")
        setBin("")
    }

    }


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new shoe</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input onChange={handleModelChange} value={model} placeholder="model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="model">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} value={manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
                    <input value ={shoe_url} onChange={handleShoeURlChange} placeholder="picture url" required type="text" name = "picture" id="picture" className="form-control"/>
                    <label htmlFor="picture">picture url</label>
                  </div>
            <div className="mb-3">
              <select value={bin} onChange={handleBinChange} required name="state" id="state" className="form-select">
                <option value="">Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>
                      {bin.bin_number}
                    </option>
                  );
                })}
              </select>
            </div>

            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}


export default ShoeForm;
