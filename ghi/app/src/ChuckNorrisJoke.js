import React, { useEffect, useState } from 'react';

function ChuckNorrisJoke() {
  const [joke, setJoke] = useState('');

  useEffect(() => {
    fetchJoke();
  }, []);

  const fetchJoke = async () => {
    try {
      const response = await fetch('https://api.chucknorris.io/jokes/random');
      if (response.ok) {
        const data = await response.json();
        setJoke(data.value);
      } else {
        console.error('Error fetching joke:', response.status);
      }
    } catch (error) {
      console.error('Error fetching joke:', error);
    }
  };

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <h2>Chuck Norris Joke</h2>
      <p>{joke}</p>
      <button onClick={fetchJoke}>Get Another Joke</button>
    </div>
  );

}

export default ChuckNorrisJoke;
